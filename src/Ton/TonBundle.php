<?php
	namespace Ton;
	use Ton\Event\TonEvent;
	use Ats\Bundles\BaseBundle;
	use LayerC\methods\RawCode;

	class TonBundle extends BaseBundle
	{

		public function __construct()
		{
			new RawCode();
		}
	}
?>