<?php
namespace Ton\Entity;

use Ton\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ton\Entity\Tweet
 *
 * @ORM\Table(name="tweet", indexes={@ORM\Index(name="hashtag", columns={"hashtag_id"})})
 * @ORM\Entity
 */
class Tweet extends Entity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tweet", type="text", nullable=false)
     */
    private $tweet;

    /**
     * @var integer
     *
     * @ORM\Column(name="tweet_id", type="integer", nullable=false)
     */
    private $tweetId;

    /**
     * @var \Hashtag
     *
     * @ORM\ManyToOne(targetEntity="Ton\Entity\Hashtag", inversedBy="tweets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hashtag_id", referencedColumnName="id")
     * })
     */
    private $hashtag;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tweet
     *
     * @param string $tweet
     * @return Tweet
     */
    public function setTweet($tweet)
    {
        $this->tweet = $tweet;

        return $this;
    }

    /**
     * Get tweet
     *
     * @return string 
     */
    public function getTweet()
    {
        return $this->tweet;
    }

    /**
     * Set tweetId
     *
     * @param integer $tweetId
     * @return Tweet
     */
    public function setTweetId($tweetId)
    {
        $this->tweetId = $tweetId;

        return $this;
    }

    /**
     * Get tweetId
     *
     * @return integer 
     */
    public function getTweetId()
    {
        return $this->tweetId;
    }

    /**
     * Set hashtag
     *
     * @param Hashtag $hashtag
     * @return Tweet
     */
    public function setHashtag(Hashtag $hashtag = null)
    {
        $this->hashtag = $hashtag;

        return $this;
    }

    /**
     * Get hashtag
     *
     * @return Hashtag 
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }
}
