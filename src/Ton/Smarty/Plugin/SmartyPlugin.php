<?php

	function TwitterText($params, $smarty)
	{
		$string = $params['text'];
		
		$string = preg_replace(
    	'@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@',
     	'<a href="$1" target="_blank">$1</a>',
    	$string);
    	$string = preg_replace(
	    '/@([a-zäáàëéèíìöóòúùñçA-ZÁÉÍÓÚÀÈÌÒÙäëïöüÑ0-9_]*)/',
	    '<a href="http://twitter.com/$1" target="_blank">@$1</a>',
	    $string);
	    
    	$string = preg_replace(
	    '/\s*#([a-zäáàëéèíìöóòúùñçA-ZÁÉÍÓÚÀÈÌÒÙäëïöüÑ0-9_]*)/',
	    ' <a href="http://search.twitter.com/search?q=%23$1" target="_blank">#$1</a>',
	    $string);
	    
	    $string = preg_replace(
	    '/23'.$params["hashtag"].'\" target/i',
	    '23'.$params["hashtag"].'\" style="color:red; text-decoration:none; font-weight:bold" target',
	    $string);
		return $string;

	}
	function TwitterDate($string)
	{
		$date = new \DateTime($string);
		return $date->format('d-m-Y H:i:s');
	}
?>