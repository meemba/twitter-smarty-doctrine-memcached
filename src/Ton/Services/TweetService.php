<?php
	namespace Ton\Services;
	use Ats\MePhp;
	use Ton\Services\TwitterAPIExchange;
	use Ton\Entity\Hashtag;
	use Ton\Entity\Tweet;

	class Tweetservice
	{
		private static $settings = array(
	    	'oauth_access_token' => "188811579-jOqDkFt5oLLAPygMTcG1xOwCEXXN7DgFoXlsFEhJ",
	    	'oauth_access_token_secret' => "DR7PTTooUXvltsLGHHOWMFnkgwgfG6itMZaGR2PFCjsqv",
	    	'consumer_key' => "ZTCkrWLhBKc3BLzRQ7sVo4ovG",
	    	'consumer_secret' => "rJ5j1fqoyvjuy03qAHDpZJcnF1hVGchhgzb6JIubYiRGdIvWQk"
		);
		public static function getAll()
		{
			$em = MePhp::$em;
			$dql= "SELECT "; 
			$dql.=" partial h.{id, hashtag, createdAt }, ";
			$dql.=" partial t.{id, tweet } ";
			$dql.=" FROM Ton\Entity\Hashtag h ";
			$dql.=" LEFT JOIN h.tweets as t ";
			$dql.=" ORDER BY h.createdAt DESC ";
			$query = $em->createQuery($dql);
			return  $query->getArrayResult();

		}
		public static function addHashTag($hashtag)
		{

			if(preg_match('/^\s*$/', $hashtag) ||$hashtag==NULL)
			{
				return "Hashtag no puede ir vacio.";
			}
			if(!preg_match('/^(?=.{2,140}$)(#|\x{ff03}){1}([0-9_\p{L}]*[_\p{L}][0-9_\p{L}]*)$/u', '#'.$hashtag))
				return "Hashtag con mal formato.";

			$em = MePhp::$em;
			$Hashtag = $em->getRepository('Ton\Entity\Hashtag')->findOneBy(array('hashtag' => $hashtag));

			$url="https://api.twitter.com/1.1/search/tweets.json";
			$getfield=($Hashtag==FALSE)? "?q=#".$hashtag: "?q=#".$hashtag."&since_id=".$Hashtag->getLastId();
			
			$requestMethod = "GET";
			$twitter = new TwitterAPIExchange(self::$settings);
			$tags = $twitter->setGetfield($getfield)
            	->buildOauth($url, $requestMethod)
             	->performRequest();

            $tags = json_decode($tags);
            if($tags==NULL) return 'No hay nada con ese hashtag(valor nulo)';
            if(count($tags->statuses)>0) self::Save($hashtag, $tags->statuses, $Hashtag, $em);
             	
		}
		private static function Save($hashtag, $tweets, $prevHashTag, $em)
		{
			
			
			$Hashtag = ($prevHashTag!=FALSE)? $prevHashTag : new Hashtag();
			$Hashtag->setHashtag($hashtag);
			$last_id = 0;
			for($i = 0 ; $i<count($tweets); $i++)
			{
				$tweet = (array)$tweets[$i];
				//print_r($tweet);				
				
				$Tweet = new Tweet();
				$Tweet->setTweet(serialize($tweet));				
				$last_id = ($tweet['id']>$last_id)? $tweet['id']:$last_id;
				$Tweet->setTweetId($tweet['id']);
				$Tweet->setHashtag($Hashtag);
				
				$em->persist($Tweet);
			}
			$Hashtag->setLastId($last_id);

			$em->persist($Hashtag);
			try
			{
				$em->flush();
				$em->clear();
			}catch(\Exception $e)
			{
				Throw new \Exception("Error en la inserción de tu busqueda. Habla con el webmaster para ver que pasa.");
			}
			

		}
	}
?>