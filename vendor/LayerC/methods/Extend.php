<?php
	namespace LayerC\methods;

	use LayerC\methods\IMethod;
	use LayerC\lib\piece\Methods;

	use LayerC\lib\Loader;
	use LayerC\lib\Lexer;
	use LayerC\methods\Variables;

	class Extend implements IMethod
	{

		private $code, $html, $match, $methods, $piece, $pairs;
		private $pieces = [
			'piece_start'=>'/^\{\%\s*piece\s*\'(.+)\'\s*\%\}/',
			'piece_end'=>'/^\{\%\s*endpiece\s*\%\}/'
		];
		private $base;
		public function __construct()
		{
			$this->base = ['extends'=>[
				"class"  =>  $this, 
				"method" => 'Execute',
                "pattern" => '/\{\%\s*extends\s*\'(.+)\'\s*\%\}/'
			]];

		}
				
		public function Execute($code, $load, $tags)
		{
			/*$this->code = $code;
			$this->piece = $tags;
			$this->html = Loader::load($load);
			$lexer = new Lexer($this->html);
			$this->methods = $lexer->get();			
			
			$this->Combine();
			return $this->get();
			*/
		}
		public function get(){return $this->html;}

		
		private function Combine()
		{
			$pairs = $this->MakePairs();
			foreach ($this->methods['PRIVATE'] as $key=>$val) 
			{				
				foreach ($pairs as $pair) 
				{	
					
					if(isset($pair['start']))
					{
						if(preg_match('/'.preg_quote($pair['start']['found'], '/ ').'/', $val['text']))
						{	
							$next = $this->methods['PRIVATE'][$key+1];							
							$replacement[] = $this->GetReplace($pair['start'], $pair['end']);
							$length = (($next['start']+$next['length'])-$val['start']);
							$pattern[] = '/'.preg_quote(substr($this->html, $val['start'], $length)).'/';							
						}	
					}
				}
			}			
			$this->html = preg_replace($pattern, $replacement, $this->html);
		}
		private function MakePairs()
		{
			$pairs = array();//founded pieces
			if($this->piece == NULL) return $pairs;
			$c=0;//counter for found
			
			foreach($this->piece as $item)
			{
				
				if(preg_match_all($this->pieces['piece_start'], $item['text'], $m))
				{
					
					$item['text'] = $m[0][0];
					$f[$c]['start']['item'] = $item;
					$f[$c]['start']['found'] = $m[1][0];					
					
				}
				if(preg_match($this->pieces['piece_end'], $item['text'])){
					$f[$c]['end']=$item;
					$c++;
				}
			}
			return $f;
		}
		private function GetReplace($start, $end)
		{
			
			$_start = $start['item']['start']+$start['item']['length'];
			$_end = $end['start']-$_start;
			
			return substr($this->code, $_start, $_end);			
		}

	}
?>