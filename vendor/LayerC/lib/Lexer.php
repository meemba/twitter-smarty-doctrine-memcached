<?php
namespace LayerC\lib;
use LayerC\lib\Loader;
class Lexer
{
	private $found, $html, $parent=NULL, $generateHTML='', $cursor, $end;

	private $regex, $positions, $positionsbegin, $tokens;
	private $state, $position = -1;
	const STATE = 0;
	const TAGS 	= -1;
	const PIECE = -2;
	private $tags = array(
		'VALUE'	=> array('{{', '}}'),
		'METHOD'	=> array('{%', '%}')

	);
	public function __construct($html)
	{
		$this->html = $html;
		$this->cursor = 0;
		$this->regex=array(
			'METHODS'		=> '/.+'.preg_quote($this->tags['METHOD'][1], '/').'/',
			'TAGS' 			=> '/.+'.preg_quote($this->tags['VALUE'][1], '/').'/',			
			'lex_tokens_start'=> '/('.preg_quote($this->tags['VALUE'][0], '/').'|'.preg_quote($this->tags['METHOD'][1], '/').'|'.preg_quote($this->tags['VALUE'][1], '/').'|'.preg_quote($this->tags['METHOD'][0], '/').')?/s'
		);

		$this->end=strlen($this->html);
		$this->make();
		
	}
	public function get($name = NULL)
	{
		if($name==NULL) return $this->tokens;
		return (isset($this->tokens[$name])) ? $this->tokens[$name] : NULL;

	}
	private function make()
	{

		preg_match_all(
			$this->regex['lex_tokens_start'], 
			$this->html, $matches, PREG_OFFSET_CAPTURE);
		;
		$this->positions = $matches[1];		
		//print_r($this->positions);
		for($t=0; $t<count($this->positions); $t++)
		{
			if(isset($this->positions[$t][0]))
			{
				if($this->positions[$t][0]==$this->tags['VALUE'][0])
					$startValue = $this->positions[$t]; //$this->positions[$t][1]; 
				if($this->positions[$t][0] == $this->tags['METHOD'][0])
					$startMethod = $this->positions[$t];//$this->positions[$t][1]; 
				if($this->positions[$t][0] == $this->tags['VALUE'][1])
				{
					$lenght = ($this->positions[$t][1]-$startValue[1])+2;
					$this->addValue($startValue[1], $lenght);
				}
				if($this->positions[$t][0] == $this->tags['METHOD'][1])
				{
					if(isset($startMethod))
					{
						$lenght = ($this->positions[$t][1]-$startMethod[1])+2;
						$this->addMethod($startMethod[1], $lenght);	
					}
					
				}
			}
		}	
		//print_r($this->tokens);
	}
	

	private function addValue($start, $lenght)
	{
		$this->tokens["TAGS"][]=array(
			"start"=>$start,
			"text"=>substr($this->html, $start, $lenght),
			"length" => $lenght
		);
	}
	private function addMethod($start, $lenght)
	{	
		
		$this->tokens["PRIVATE"][]=array(
			"start"=>$start,
			"text"=>substr($this->html, $start, $lenght),
			"length" => $lenght
		);	
		/*if(preg_match($this->regex['METHODS'], $this->html, $match, null, $this->cursor))
		{
			
			preg_match_all('/^(\{%\s*piece\s*\'.*\'%\})/', $match[0], $clean);
			
			$this->tokens["PRIVATE"][]=array(
				"start"=>$this->cursor,
				"text"=>$match[0],
				"length" => strlen($match[0])
			);
			$this->moveCursor($match[0]);	
		}*/
	}
	private function moveCursor($val){
		$this->cursor += strlen($val);
	}
}
?>