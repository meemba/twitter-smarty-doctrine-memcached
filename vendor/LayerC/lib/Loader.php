<?php
namespace LayerC\lib;

class Loader
{
    private static $basePath = '', $bundlePath = '';


    public static function BasePath($basePath, $bundlePath)
    {
    	self::$basePath = $basePath;
        self::$bundlePath = $bundlePath;
    }
    public static function load($tpl)
    {
        
        $bundlePath = self::$bundlePath.$tpl;
        if(preg_match_all('/([A-Za-z]*)\:([A-Za-z\.]*)/', $tpl, $m))
            $bundlePath = $m[1][0].'/view/'.$m[2][0];
        $file = file_get_contents(self::$basePath.$bundlePath);
        return $file;
    }

}
?>