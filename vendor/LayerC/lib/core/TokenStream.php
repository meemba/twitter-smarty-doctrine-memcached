<?php
	namespace LayerC\lib\core;
	use LayerC\lib\core\LayerCToken;
	class TokenStream
	{
		private $lexer, $tokens,
		$pushed = [];
		public $eof = FALSE, $current;
		public function __construct($lexer)
		{
			$this->lexer = $lexer;
			$this->next();
		}
		public function next()
		{
			
			if(!empty($this->pushed))			
				$token = array_shift($this->pushed);
			else
				$token = $this->lexer->nextToken();

			
			
			$old = $this->current;
			$this->current = $token; 
			$this->eof = $token->type === LayerCToken::EOF_TYPE;
			return $old;
		}
		public function expect($first, $second=NULL)
		{
			$token = $this->current;
			

			if(!$token->test($first, $second))
				throw new \Exception("ERRor find in line : ".$token->lineo);
			
			$this->next();
			return $token;
		}
		private function look()
		{

		}
		
	}
?>