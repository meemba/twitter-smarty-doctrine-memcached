<?php
	namespace LayerC\lib\core;

	use LayerC\lib\piece\Methods;
	use LayerC\lib\Lexer;
	use LayerC\lib\core\LayerCLexer;
	use LayerC\lib\core\LayerCToken;
	use LayerC\lib\core\TokenStream;
	use LayerC\lib\piece\LayerCText;
	////////////////////////////////
	

	class GenerateHtml
	{
		private $code, $html="", $lexer, $tokens=[],
		$tags, $args, $route, $codes = [], $stream,
		$handlers, $extends, $block, $pieces, $parser, $p = 0;
		private $blocks = array();

		public function __construct($code, $route, $args)
		{
			$this->html = $code;
			$this->code = $code;

			$this->args = $args;
			$this->route = $route;
			$this->handlers
			 = [
				'extends'=>[$this, 'parseExtends'],
				'piece'=>[$this, 'parsePiece']
			];
		}
		public function Methods()
		{


			$methods = new Methods();

			/*$methods = new LayerCMethods(
				$this->html, 
				$this->Lexer($this->code, 'PRIVATE'), 
				$this->route, 
				$this->args
			);
			$this->html = $methods->getHtml();
			*/
		}
		public function getParsed()
		{
			return $this->parser;
		}
		public function getPieces()
		{
			return $this->pieces;
		}
		public function Values()
		{
			$functions = new LayerCFunctions($code, $this->Lexer($code, 'TAGS'), $args);
			$code = $functions->getHtml();
		}
		public function parseExtends($token)
		{
			if(!is_null($this->extends)) throw new \Exception("solo un extends por pagina please");
			//vale tengo un extends 
			//pero me hace falta el valor del extends el text???(valor de tpl.string)
			$this->extends = $this->stream->expect(LayerCToken::STRING_TYPE)->value;
			$this->stream->expect(LayerCToken::BLOCK_END);

		}
		public function parsePiece($token)
		{	///ahora quiero las pieces .
			$name = $this->stream->expect(LayerCToken::STRING_TYPE)->value;
			$this->stream->expect(LayerCToken::BLOCK_END);
			$this->blocks[] = ['type'=>'piece', 'name'=>$name];		
			$this->pieces[$this->p++]['block'] = $name;
		}
		public function generate()
		{
			$this->lexer = new LayerCLexer($this->code);
			$this->stream =  new TokenStream($this->lexer);
			$this->parse();
		}
		public function LoadExtends()
		{
			return $this->extends;
		}
		public function parse($next=FALSE)
		{
			while(!$this->stream->eof)
			{
				switch ($this->stream->current->type) 
				{

					case LayerCToken::BLOCK_START:
						//print_r($this->stream->current);
						$this->stream->next();
						$token = $this->stream->current;				
						
						if ($token->type !== LayerCToken::NAME)
							throw new \Exception('expected directive',
					                           $token->lineo);
						///encuenra sobre el siguiente:
						
						if(!isset($this->handlers[$token->value]))
						{
							$last = array_pop($this->blocks);
							
							if('end'.$last['type']==$token->value)
							{
								$cur = $this->stream->next();
								$this->pieces[$this->p++]['endblock'] = $last['name'];
								$this->stream->expect(LayerCToken::BLOCK_END);
							}
							else 	
								throw new  \Exception($token->value);
						}else{
							$this->stream->next();
							$node = call_user_func($this->handlers[$token->value], $token);
						}

						break;
					case LayerCToken::TEXT:
						$token = $this->stream->next();
						$rv[] = new LayerCText($token->value, $token->lineo);
						///$this->parser[$this->p]['text'] = $token->value;						
						$this->pieces[$this->p++]['text'] = $token->value;
						

						break;
					case LayerCToken::NAME:
						$token = $this->stream->next();
						$node = call_user_func($this->handlers[$token->value], $token);
						break;
					case LayerCToken::VARIABLE_START:
						$this->stream->next();
						$token = $this->stream->next();
						$this->pieces[$this->p++]['variable'] = $token->value;
						$this->stream->next();
						break;	
					default:
						# code...
						break;
				}			
			}			
		}
		/////////////////lexico
		private function Lexer($code, $what)
		{
			$c = new Lexer($code);	
			return $this->tags = $c->get($what);
		}
	}
?>