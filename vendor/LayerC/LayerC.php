<?php
/**
* Self engine template for php
*/
namespace LayerC;
use LayerC\lib\Translate;
use LayerC\lib\Loader;

class LayerC
{
	private $route, $cache, $pospiece = 0, $vars=NULL;
    public function __construct(&$tpl, $route, $cache)
    {   
        $tpl = $this;
        $this->route = $route;
    }
    public function basePath($basepath, $bundlePath)
    {
        Loader::BasePath($basepath, $bundlePath);
    }
    public function render($tpl, $vars=NULL)
    {
        $this->vars = $vars;
    	$translate = new Translate(Loader::load($tpl), $this->route, $vars);
        $translate->generate();
        //echo "<pre>";
        $piece = 0;
        //print_r($translate);
        if(($extends = $translate->LoadExtends())!=NULL)
        {
            $etranslate  = new Translate(Loader::load($extends), $this->route, $vars);
            $etranslate->generate();
            $extended = $etranslate->getPieces();
            $pieces = $translate->getPieces();
            $html="";
            //print_r($extended);
            //print_r($pieces);
            $piece = FALSE;
            for($t=0; $t<count($extended); $t++)
            {
                if(isset($extended[$t]['text']))
                    $html.= $extended[$t]['text'];
                if(isset($extended[$t]['variable']))
                    $html.=  $this->vars[$extended[$t]['variable']];   
                if(isset($extended[$t]['block']))
                {                   
                    $html.=$this->GetPiece($extended[$t]['block'], $pieces);
                    if(strlen($html)>0) $t++;                    
                }
            }
        }
        echo str_replace('\\n', PHP_EOL, $html);
    }
    public function GetPiece($name, $arr, $html="")
    {
        for($i=$this->pospiece; $i<count($arr); $i++)
        {
            if(isset($arr[$i]['piece']))
                if($arr[$i]['block']==$name) {
                    $this->pospiece = $i;
                    $this->GetPiece($name, $arr, '');        
                }
            
            if(isset($arr[$i]['endblock']))
                if($arr[$i]['endblock']==$name) {
                    $this->pospiece = $i;
                    return $html;
                }
            if(isset($arr[$i]['text']))
            {
                $key = key($arr[$i]);
                $html.=  $arr[$i][$key];
            }
            if(isset($arr[$i]['variable']))
                $html.=  $this->vars[$arr[$i]['variable']];   
            
            
        }       
    }
}
?>