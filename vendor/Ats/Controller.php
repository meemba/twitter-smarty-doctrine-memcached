<?php
	namespace Ats;
	use Ats\MePhp;

	class Controller
	{
		private $em, $tpl;

		public function __construct($em , $tpl)
		{
			$this->em = $em;
			$this->tpl = $tpl;
		}
		protected function get($name)
		{
			switch ($name) {
				case 'doctrine':
					return $this->em;
					break;
				
				default:
					throw new \Exception("No hay ningun modulo, variable, con ese nombre ".$name);
					break;
			}
		}	
		protected function render($tpl, array $vars=[])
		{
			$path = preg_split('/\\\/', get_class($this)) ;
			$this->tpl->basePath(__DIR__.'/../../src/', $path[0].'/view/');
			$this->tpl->render($tpl, $vars);			
		}
	}
?>