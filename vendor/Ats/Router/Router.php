<?php
	namespace Ats\Router;

	use Ats\Router\BaseRouter;
	use Ats\Ephp\Ephp;
	use Ats\Router\Route;


	class Router extends BaseRouter
	{
		private static $ins = NULL;

		public function __construct()
		{
			parent::__construct();
		}
		
		public function add($uri, $route)
		{
			
			if(is_array($route))
			{
				$route = new Route($uri, $route);
			}			
			$this->AddRoute($uri, $route);
			return $this;
		}

		public function get($uri, $route)
		{			
			$route->setMethod('get');
			$this->AddRoute($uri, $route);
			return $this;
		}		
		//////////////////////////////////////////////////////////
		public static function init()
		{
			if(self::$ins==NULL) self::$ins = new Router();
			return self::$ins;	
		}  

	}
?>