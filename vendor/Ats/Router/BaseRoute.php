<?php

	namespace Ats\Router;

	trait BaseRoute
	{
		private $method;
		
		public function getMethod()
		{
			return $this->method;
		}
		public function setMethod($value)
		{
			if(is_array($value))
			{
				for($t=0; $t<count($value); $t++)
					$this->method[] = strtolower($value[$t]);
			}
			if(is_string($value))
			{
				$value = strtolower($value);
				$methods = preg_split('/\,/', $value);
				for($t=0; $t<count($methods); $t++)
					$this->method[] = trim($methods[$t]);
			}
			
		}
		public function getUri()
		{
			return $this->uri;
		}
		public function getController()
		{
			return $this->controller;
		}
		public function getDefaults()
		{
			if($this->inject)			
				foreach($this->inject as $key=>$val)				
					$this->defaults[$key] = $this->regExp->setData($val);
				
			
			return $this->defaults;
		}

	}
?>