<?php
	namespace Ats\Router;
	class BaseRouter
	{
		private $method;
		private $uri;
		private $SERVER;
		private $routes;

		public function __construct()
		{
			$this->SERVER = $_SERVER;
			$this->uri = $_SERVER['REQUEST_URI'];
			$this->method = strtolower($_SERVER['REQUEST_METHOD']);
		}
		public function AddRoute($uri, $route)
		{			
			$this->routes[$uri] = $route;
		}
		public function Find()
		{

			$max = 0; $route =null;
		
			foreach($this->routes as $_route)
			{
				$u = $_route->Match($this->uri);
				if($u>=$max) {
					$route = $_route;
					$max = $u; 
				}
			}
			if($max==0)
				throw new\Exception('page 404');

			if(!in_array($this->method, $route->getMethod()))
				throw new\Exception('page 404 - mal metodo (post, get, put, delete)');
			

			return $route;
		}
		public function getMethod()
		{

		}
	}
?>