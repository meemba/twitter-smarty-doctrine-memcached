<?php
	namespace Ats\Router;

	class Regxer
	{
		private $URI;
		private $routeURI;

		public function __construct($URI=NULL, $routeURI=NULL)
		{
			if($URI!=NULL) $this->URI = $URI;
			if($routeURI!=NULL) $this->routeURI = $routeURI;
		}
		public function setData($value)
		{
			
			if(preg_match_all('/^class\:(.*)$/', $value, $m))
				return new $m[1][0];
			
		} 
		public function getPartsURI($uri)
		{
			return preg_split('/\//', $uri);
		}
		public function getOptional($uri)
		{
			preg_match_all('/\{([a-z][a-zA-Z0-9_,]*)\}/', $uri, $m);
			return $m;
		}


	}
?>