<?php
	namespace Ats\Ephp;
	use Ats\Ephp\Ephp;
	use Yaml\Yaml;

	class AtsEphp
	{
		private $ephp, $config;

		public function __construct($bundles)
		{
			$this->config = Yaml::parse(file_get_contents(__DIR__.'/../../../app/config/config.yml'));
			if(isset($this->config['_memcache']) && $this->config['_memcache'])
			{
				$config = $this->config['_memcache'];
				$memcached = new \Memcached();
    			$memcached->addServer($config['server'], $config['port'], $config['weight']);

    			if(!($this->memcached = $memcached->get('ephp')))
    			{
    				$ephp = new Ephp($bundles, $this->memcached); 	
    				$memcached->set('ephp', $ephp);
    				$memcached->set('tpl', $ephp->getTPL());
    			}
    			$this->ephp = $memcached->get('ephp');
    			$this->ephp->setTPL($memcached->get('tpl'));
			}else{
				$this->ephp = new Ephp($bundles, FALSE);					
			}		
		}
		public function config()
		{
			$this->ephp->config();
		}
		public function start()
		{
			$this->ephp->start();
		}
	}
?>