<?php
	namespace Ats\Ephp;
	use Ats\Event\Event;
	use Ats\Event\Events;
	use Ats\Event\EventCollection;
	use Ats\Ephp\BaseEphp;
	use Ats\Ephp\RunEphp;
	use Ats\Ephp\ConfigEphp;
	use Yaml\Yaml;
	

	class Ephp
	{
		use BaseEphp;
		use ConfigEphp;
		use RunEphp;

		private $_this=NULL, $memcache = FALSE, $route = NULL;		

		public function __construct($bundles, $memcache)
		{
			$this->memcache = $memcache;
			$this->config = Yaml::parse(file_get_contents(__DIR__.$this->configFolder.'config.yml'));
			$this->bundles = $bundles;
			$this->eventCollection = new EventCollection();
			$this->setTPL();
			$this->setORM();
		}				
		//
		public function start()
		{
			
			$this->config();
			$this->route = $this->Router->Find();
			$this->run();
		}
	}
?>