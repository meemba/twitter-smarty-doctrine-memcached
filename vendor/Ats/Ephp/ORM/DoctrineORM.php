<?php
	namespace Ats\Ephp\ORM;

	use Doctrine\ORM\Tools\Setup;
	use Doctrine\Common\Proxy\AbstractProxyFactory;
	use Doctrine\ORM\EntityManager;

	class DoctrineORM
	{
		public function __construct(&$orm, $configEphp)
		{
			$paths = array(__DIR__."/../../../../src/Ton/Entity");
        	$isDevMode = false;
        	$config = Setup::createAnnotationMetadataConfiguration
        		($paths, false, null, null, false);
        	$mode = AbstractProxyFactory::AUTOGENERATE_EVAL;
        	$config->setAutoGenerateProxyClasses($mode);
        	$orm = EntityManager::create($configEphp['doctrine'], $config);

		}
	}
?>