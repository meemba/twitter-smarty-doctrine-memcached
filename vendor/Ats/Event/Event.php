<?php
	namespace Ats\Event;

	use Ats\Event\Events;
	class Event 
	{
		private $name, $assoc;
		public function __construct($name, $assoc=NULL)
		{
			$this->name = $name;
			$this->assoc = ($assoc==NULL) ? Events::basic : $assoc;
		}
		public function getName()
		{
			return $this->name;
		}
		public function getAssoc()
		{
			return $this->assoc;
		}
	}
?>