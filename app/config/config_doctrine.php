<?php
	use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;

        $paths = array(__DIR__."/../../src/Ton/Entity");
        $isDevMode = false;

        // the connection configuration
        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '33dead',
            'dbname'   => 'db_critizen',
        );

        $config = Setup::createAnnotationMetadataConfiguration($paths, false, null, null, false);
        $mode = Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_EVAL;
        $config->setAutoGenerateProxyClasses($mode);
        $entityManager = EntityManager::create($dbParams, $config);


        $memcached = new \Memcached();
        $memcached->addServer('127.0.0.5', 11211);

        $cacheDriver = new \Doctrine\Common\Cache\MemcachedCache();
        $cacheDriver->setMemcached($memcached);
        $cacheDriver->save('cache_id', 'my_data');

?>