<?php
	$smarty = new Smarty();
	$smarty->caching_type = 'memcache';
	$smarty->setTemplateDir(__DIR__.'/../../src/Ton/view');
	$smarty->setCompileDir(__DIR__.'/../../src/Ton/view/templates_c');
	$smarty->setCacheDir(__DIR__.'/../../app/cache/smarty');
	require_once __DIR__.'/../../src/Ton/Smarty/Plugin/SmartyPlugin.php';
	$smarty->registerPlugin("function","twitter_text", "TwitterText");
	$smarty->registerPlugin("modifier","twitter_date", "TwitterDate");
?>