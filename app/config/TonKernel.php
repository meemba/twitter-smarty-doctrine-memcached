<?php
	session_start();
    require_once __DIR__."/../../vendor/doctrine-vendor/vendor/autoload.php";
	require_once __DIR__.'/../../vendor/SplClassLoader.php';
    
    $vendors = array(
    	'Dctrine'=>__DIR__.'/../../vendor/doctrine-vendor/lib',
    	'Ats'=>__DIR__.'/../../vendor',
		'PHPRouter'=> __DIR__.'/../../vendor/Router/src',
        'LayerC'=>__DIR__.'/../../vendor',
		'Ton'=> __DIR__.'/../../src',
        'Yaml'=>__DIR__.'/../../vendor/Yaml-parser'
    );

    foreach($vendors as $key=>$val)
    {
    	$loader = new SplClassLoader($key, $val);
    	$loader->register();
    }       
    //require_once __DIR__.'/config_smarty.php';     

    $bundles = 
    [
        new Ton\TonBundle()
    ];
?>